# libaloha: Personal library to provide some classes, functions and binaries

This is just a little python package I use to centralize my dev works. Enjoy it if you want :-)

## Modules

### aloha.cache

**aloha.cache.redis** need the **redis** package installed.

### aloha.django

**aloha.django** need the **django** package installed.

### aloha.markdown

**aloha.markdown** need the **markdown** package installed.
