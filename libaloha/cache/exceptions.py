

class CacheException(Exception):
    pass


class CacheConnectionError(CacheException):
    pass
